import { ref, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/Salary'


export const useSalaryStore = defineStore('salary', () => {
  let lastIndex = 7
  let editedIndex = -1
  const dialog = ref(false)
  const reportSalaryDialog = ref(false) 
  const initSalary: Salary = {
    idSalary: -1,
    month: '',
    status: 'Not Paid',
    salarybaht: 25000
  }
  
  const editedsalary = ref<Salary>(JSON.parse(JSON.stringify(initSalary)))
  const salarys = ref<Salary[]>([
    { idSalary: 1, month: 'January', status: ' Not Paid ', salarybaht: 25000 },
    { idSalary: 2, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { idSalary: 3, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { idSalary: 4, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { idSalary: 5, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { idSalary: 6, month: 'January', status: ' Not Paid ', salarybaht: 15000 }
  ])

  function editItem(item: Salary) {
    editedIndex = salarys.value.indexOf(item)
    editedsalary.value = Object.assign({}, item)
    dialog.value = true
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedsalary.value = Object.assign({}, initSalary)
      editedIndex = -1
    })
  }
// function save() {
  //   if (editedIndex > -1) {
  //     Object.assign(salarys.value[editedIndex], editedsalary.value);
  //   } else {
  //     editedsalary.value.idSalary = lastIndex++;
  //     salarys.value.push(editedsalary.value);
  //   }
  //   editedIndex = -1;
  //   closeDialog();
  // }

function save() {
    const index = salarys.value.findIndex(salary => salary.idSalary === editedsalary.value.idSalary);
    if (index > -1) {
      salarys.value[index] = { ...salarys.value[index], ...editedsalary.value };
    } else {
      editedsalary.value.idSalary = lastIndex++;
      salarys.value.push(editedsalary.value);
    }
    closeDialog();
  }
  
  function openReportSalaryDialog(){
    reportSalaryDialog.value =!reportSalaryDialog.value
}
  
  


  return {
    editedsalary,
    salarys,
    editItem,
    dialog,
    closeDialog,
    save,
    lastIndex,
    editedIndex,
    openReportSalaryDialog,
    reportSalaryDialog
  }
})
