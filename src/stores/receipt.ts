import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'

import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { usePromotionStore } from './promotion'



export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const promotionStore = usePromotionStore()
  const receiptDialog = ref(false)
  const disMember = ref(0)
  const activeComponent = ref(1)
  const date = ref();
  const formattedDate =ref<string>('')
  
  
  const textDate = ref()
  let unit = 0
  const changeComponent = (componentNumber: number) => { activeComponent.value = componentNumber }
  const initReceipt:Receipt={
    id: -1,
    createdDate: '',
    totalBefore: 0,
    memberDiscount: 0,
    promotionDiscount: 0,
    promotion: undefined,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    user: undefined,
    userId: authStore.currentUser?.id,
    memberId: 0,
    member:undefined
  }
  const receipt = ref<Receipt>(JSON.parse(JSON.stringify (initReceipt)))
  const receiptItems = ref<ReceiptItem[]>([])

  const unitItem =ref(1)

  const receipts = ref<Receipt[]>([])
  let lastidReceipt = receipts.value.length+1

  function addReceiptItem(product: Product) {
    console.log(product);
    const index = receiptItems.value.findIndex((item) => item.product?.name === product.name &&
      item.product?.gsize === product.gsize &&
      item.product?.sweet_level === product.sweet_level &&
      item.product?.type === product.type)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
    } else {

      if (product.category != 1) {
        const newReceipt: ReceiptItem = {
          id: -1,
          name: product.name,
          type: '-',
          gsize: '-',
          sweet_level: '-',
          price: product.price,
          unit: 1,
          productId: product.id,
          product: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()

      } else {
        const newReceipt: ReceiptItem = {
          id: -1,
          name: product.name,
          type: product.type,
          gsize: product.gsize,
          sweet_level: product.sweet_level,
          price: product.price,
          unit: 1,
          productId: product.id,
          product: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()

      }
    } unit++
  }
  function editReceiptItem(p: Product, r: ReceiptItem, index: number) {
    const newReceipt: ReceiptItem = {
      id: -1,
      name: r.name,
      type: p.type,
      gsize: p.gsize,
      sweet_level: p.sweet_level,
      price: r.price,
      unit: r.unit,
      productId: r.id,
      product: r.product
    }
    receiptItems.value[index] = newReceipt
    calReceipt()
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    unit -= receiptItems.value[index].unit
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    } else {
      unit--
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    promotionStore.promotionUnit(receiptItems.value)
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + (item.price * item.unit)
    }
    receipt.value.totalBefore = totalBefore
    if (promotionStore.currentPromotion) {
      receipt.value.promotionDiscount = totalBefore * promotionStore.currentPromotion.discount
      receipt.value.promotion = promotionStore.currentPromotion
      totalBefore = totalBefore * (1 - promotionStore.currentPromotion.discount)
    }
    if (memberStore.currentMember) {
      receipt.value.total = totalBefore - disMember.value
      receipt.value.memberDiscount = disMember.value
    } else {
      receipt.value.total = totalBefore
    }
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: -1,
      createdDate: '',
      totalBefore: 0,
      memberDiscount: 0,
      promotionDiscount: 0,
      promotion: undefined,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.currentUser?.id,
      memberId: 0
    }
    unit = 0
    memberStore.clearMember()
    promotionStore.clearPromo()
    
  }

  function showReceiptDialog() {
    setDate()
    receipt.value.receiptItems = receiptItems.value
    receipt.value.createdDate = formattedDate.value
    textDate.value = receipt.value.createdDate.split(',')
    receiptDialog.value = true
  }
  function showReceiptDialog2(item: Receipt) {
    textDate.value = item.createdDate.split(',')
    receipt.value = item
    receiptDialog.value = true
  }
  function setDisMember(dis: number) {
    disMember.value = dis
    calReceipt()

  }
  function addPoint() {
    memberStore.addPoint(unit)
  }
  function deletePoint() {
    memberStore.deletePoint(unit)
  }
  function addReceipt() {
    if(memberStore.currentMember!=undefined){
      receipt.value.member = memberStore.currentMember
    }
    if(promotionStore.currentPromotion!=undefined){
      receipt.value.promotion = promotionStore.currentPromotion
    }
    if(authStore.currentUser !=undefined){
      receipt.value.user = authStore.currentUser
    }
    receipt.value.id = lastidReceipt++
    console.log(receipt.value);
    
    receipts.value.unshift(receipt.value)
    memberStore.currentMember = undefined
    promotionStore.currentPromotion = undefined
  }
  function setDate(){
    date.value = new Date();
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false, // 24 ชั่วโมง
    });
    textDate.value = ref(formattedDate.value.split(','))
  }
  function closeDialog(){
    receiptDialog.value = false
    receipt.value = JSON.parse(JSON.stringify (initReceipt))
  }
  return {
    receiptItems, receipt, receiptDialog, activeComponent, receipts,textDate,lastidReceipt,unitItem,
    addReceiptItem, removeReceiptItem, inc, dec, calReceipt, editReceiptItem, clear, showReceiptDialog, setDisMember, addPoint, deletePoint, changeComponent, addReceipt, showReceiptDialog2,closeDialog
  }
})
