import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import type { Stock } from '@/types/Stock'
import type { ReceiptStockItem } from '@/types/ReceiptStockItem'
import type { ReceiptStock } from '@/types/ReceiptStock'
import { useAuthStore } from './auth'
import axios from 'axios';
import http from '@/services/http'
import { useLoadingStore } from './loading'
import stockService from '@/services/stock'

export const useStockStore = defineStore('stock', () => {
const dialogS = ref(false)
const formS = ref(false)
const dialogE =ref(false)
const dialogDelete = ref(false) 
const dialogCheckStock = ref(false) 
const dialogPrint = ref(false) 
const dialogImport = ref(false)
const dialogShowReceipt =ref(false)

const authStore= useAuthStore()
// const stockItems = ref<Stock[]>([
//     { id: 1, name: 'ไซรัป', minimum: 10, balance:15,unit:'ขวด', price:225},
//     { id: 2, name: 'เมล็ดกาแฟ', minimum: 10, balance:4,unit:'กิโลกรัม', price:400},
//     { id: 3, name: 'ผงชาไทย', minimum: 10, balance:6,unit:'กิโลกรัม', price:300},
//     { id: 4, name: 'ผงโกโก้', minimum: 10, balance:5,unit:'กิโลกรัม', price:350},
//     { id: 5, name: 'ผงชาเขียว', minimum: 10, balance:8,unit:'กิโลกรัม', price:360},
//     { id: 6, name: 'นมจืด', minimum: 10, balance:3,unit:'กิโลกรัม', price:120},
//     { id: 7, name: 'น้ำเชื่อม', minimum: 10, balance:8,unit:'ลิตร', price:250},
//     { id: 8, name: 'วิปปิ่งครีม', minimum: 10, balance:11,unit:'ลิตร', price:150},
//     { id: 9, name: 'น้ำผึ้ง', minimum: 10, balance:14,unit:'ลิตร', price:95},
//     { id: 10, name: 'ครีมเทียม', minimum: 10, balance:12,unit:'ลิตร', price:120},
//     { id: 11, name: 'น้ำตาลคาราเมล', minimum: 10, balance:4,unit:'กิโลกรัม', price:120},
//     { id: 12, name: 'ไข่มุก', minimum: 10, balance:6,unit:'กิโลกรัม', price:35},
//     { id: 13, name: 'เมล็ดมะขามคั่ว', minimum: 10, balance:10,unit:'กิโลกรัม', price:250},
//     { id: 14, name: 'กีวี', minimum: 10, balance:11,unit:'กิโลกรัม', price:150}
//     ])
const res = http.get('/stocks')
const stockItems = ref<Stock[]>([])
const initSProduct:Stock = {
      id: -1, 
      name: '', 
      minimum:0, 
      balance:0,
      unit:'', 
      price:0
  
    }
const editedSProduct = ref<Stock>(JSON.parse(JSON.stringify (initSProduct)))
const stockLows = ref<Stock[]>([])
const editedLows = ref<Stock>(JSON.parse(JSON.stringify (initSProduct)))
let editedIndex = -1
let lastIndex = 15
const unitchange =ref(0)
const listCheck:number[]= Array.from({length:stockItems.value.length},()=>0);
const loadingStore = useLoadingStore()
const receiptStockS = ref<ReceiptStock[]>([])
const receiptStockItems = ref<ReceiptStockItem[]>([])
const listReceipt:number[]= Array.from({length:stockItems.value.length},()=>0);

let lastIdShow = receiptStockS.value.length+1

const receiptStock = ref<ReceiptStock>({
id: -1,
idReceipt: '',
nameplace: '',
createdDate: new Date(),
totalBefore: 0,
total: 0,
receivedAmount: 0,
paymentType: '',
userId: 0,

}
)
  async function getStocks(){
  const res = await stockService.getStocks()
  stockItems.value = res.data
}

  async function saveStocks(stock:Stock){
     loadingStore.doload()
  if(stock.id <0){  //Add
    closeDialogAdd()
    const res = await stockService.addNew(stock)
  }else{            //Update
    closeDialog()
 const res = await stockService.update(stock)
 
  }
   await getStocks()
   loadingStore.fisnish()
}
  async function deleteStock(stock:Stock){
    loadingStore.doload()
  const res = await stockService.remove(stock)
  await getStocks()
  loadingStore.fisnish()
}


function editSProduct(s:Stock){
         console.log('Open Edit dialog');
          editedIndex = stockItems.value.indexOf(s)
          editedSProduct.value = Object.assign({}, s)
          dialogE.value = true
          formS.value=true
  }
  function addreceiptStock(){
    console.log('Push ');
    // saveShow()

    receiptStock.value.id = lastIdShow++
   receiptStock.value.user = authStore.currentUser!;
    receiptStockS.value.unshift(receiptStock.value)
   
    console.log("ReceiptS"+receiptStockS.value);
    
    console.log('ReceiptSTOCK '+ receiptStock.value.receiptStockItems);
    console.log('User in re '+ receiptStock.value.user )
 
}

  
  function deleteSProduct(s:Stock){
        editedIndex = stockItems.value.indexOf(s)
        editedSProduct.value = Object.assign({}, s)
        dialogDelete.value = true
  }
  function  closeDelete () {
          console.log('Closing dialog');
          dialogDelete.value = false
          nextTick(() => {
              editedSProduct.value = Object.assign({}, initSProduct)
          })
        }
     function closeDialog(){
         console.log('Close')
          dialogE.value = !dialogE.value 
          
          nextTick(() => {
            editedSProduct.value = Object.assign({}, initSProduct)
            editedIndex = -1
          })
        }
        function closeDialogT(){
          console.log('Close')
          editedSProduct.value = Object.assign({}, initSProduct)
          editedIndex = -1
          dialogE.value = !dialogE.value 
         
         }

        function closeDialogAdd(){
          dialogS.value = false
       
          nextTick(() => {
            editedSProduct.value = Object.assign({}, initSProduct)
            editedIndex = -1
          })
        }
        function closeCheckDialog(){
          console.log('CloseCheck')
          dialogCheckStock.value = !dialogCheckStock.value
          nextTick(() => {
            editedLows.value = Object.assign({}, initSProduct)
            editedIndex = -1
          })
        }
  function  deleteItemConfirm () {
        console.log('Delete item');
          // delete item from List
          deleteStock(editedSProduct.value)
          closeDelete()
        }
       
        async function save(){
          await saveStocks(editedSProduct.value)
         
          
        }

        const newEdId = ref<ReceiptStock[]>([]) 
      
         
        function openAddDialog (){
            dialogS.value = !dialogS.value
        }   
      function minimumStock(){
        stockLows.value =[]
        for(let index = 0; index < stockItems.value.length; index++){
          if(stockItems.value[index].balance<stockItems.value[index].minimum){
            stockLows.value.push(stockItems.value[index])
          }
        
        } 
      }

     
      function addBalance(){
        console.log('Add balance');
        for (let index = 0; index < listReceipt.length; index++) {
          const u =  Number(listReceipt[index])
          stockItems.value[index].balance =u
      }
      
  }

    function editCheck(s:Stock){

       editedIndex = stockItems.value.indexOf(s)
       editedSProduct.value = Object.assign({}, s)
}
    function saveCheck(){
      console.log('Closed man');
      Object.assign(stockItems.value[editedIndex], editedSProduct.value)
      closeCheckDialog()
      
    }
    function logStupid(){
      console.log(listCheck);
      
    }
    function logIntplus(){
      for (let index = 0; index < listCheck.length; index++) {
         const u =  Number(listCheck[index])
         stockItems.value[index].balance =u
      }
      closeCheckDialog()
    }

    function openPrint(){
           dialogCheckStock.value =!dialogCheckStock.value
           dialogPrint.value =!dialogPrint.value
    }
    function openCheckDialog(){
      dialogCheckStock.value =!dialogCheckStock.value
}
    function openImportDialog(){
          dialogImport.value = !dialogImport.value
          minimumStock()
      }
    const dialogImportM = ref(false)
function openImportManage(){
  dialogImport.value=!dialogImport.value
    dialogImportM.value =!dialogImportM.value
    
    minimumStock()
}
function addStockItem(s:Stock){
      console.log(s);
      const index = receiptStockItems.value.findIndex((item) => item.stock?.name === s.name && 
                                                        item.stock?.minimum ===s.minimum && 
                                                        item.stock?.price ===s.price &&
                                                        item.stock?.unit ===s.unit  )
    if (index >= 0) {
        receiptStockItems.value[index].unitPerItem++
       
    } else {
        const newStockReceipt: ReceiptStockItem = {
          id: -1,
          name: s.name,
          minimum: s.minimum,
          balance: s.balance,
          unit: s.unit,
          price: s.price,
          totalprice: 0,
          unitPerItem: 1,
          stockId: s.id,
          stock: s,
          beforeprice: 0
        }
      receiptStockItems.value.push(newStockReceipt)
      
      
      calReceipt()
}}
function calReceipt(){
  let total =0
  for(const item of receiptStockItems.value){
    total = total+Number(item.totalprice)
}
receiptStock.value.total = total

}
function enterPrice(s:ReceiptStockItem){
  console.log('Enter',s.beforeprice);
  const index = receiptStockItems.value.findIndex((item) => item.stock?.name === s.name && 
                                                        item.stock?.minimum ===s.minimum && 
                                                        item.stock?.price ===s.price &&
                                                        item.stock?.unit ===s.unit )
  receiptStockItems.value[index].totalprice = s.beforeprice
  calReceipt()
  receiptStockItems.value[index].beforeprice =0

 
}
function clearPrice(s:ReceiptStockItem){
  // console.log('Enter',s.totalprice);
  const index = receiptStockItems.value.findIndex((item) => item.stock?.name === s.name && 
                                                        item.stock?.minimum ===s.minimum && 
                                                        item.stock?.price ===s.price &&
                                                        item.stock?.unit ===s.unit )
  receiptStockItems.value[index].totalprice = 0

  
}

function addBTest(s:ReceiptStockItem){
  console.log('Enter Balance',s);
  const index = stockItems.value.findIndex((item) => item.id === s.stockId )
  receiptStock.value.receiptStockItems = receiptStockItems.value
  console.log('Index '+index);
  stockItems.value[index].balance =s.unitPerItem
 
 
 

}

function clear(){
   receiptStockItems.value = []
   receiptStock.value ={
    id: -1,
    idReceipt: '',
    nameplace: '',
    createdDate: new Date(),
    totalBefore: 0,
    total: 0,
    receivedAmount: 0,
    paymentType: '',
    userId: 0
   }
}
function closeImportM(){
  console.log('Close import M');
  dialogImportM.value = !dialogImportM.value
  
}


  return {stockItems,initSProduct,editedSProduct,dialogE,formS,dialogDelete,dialogS,dialogCheckStock,stockLows,unitchange,listCheck,dialogPrint,dialogImport
    ,dialogImportM,receiptStockItems,receiptStock,receiptStockS,dialogShowReceipt
    ,openImportManage,closeDelete,save,openAddDialog,deleteItemConfirm,deleteSProduct,editSProduct,closeDialogAdd,minimumStock,addBalance,saveCheck,editCheck,logStupid,logIntplus
    ,openPrint,openImportDialog,openCheckDialog,addStockItem,enterPrice,clear,closeImportM,addBTest,addreceiptStock,closeDialog,closeDialogT,
    clearPrice,getStocks
      }})
