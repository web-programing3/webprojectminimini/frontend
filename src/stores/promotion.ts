import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'

export const usePromotionStore = defineStore('promotion', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const form = ref(false)
  const currentPromotion = ref<Promotion | null>()
  const disMax = ref(0)

  let editedIndex = -1

  const initPromotion: Promotion = {
    id: -1,
    name: '',
    con: '',
    start: '',
    end: '',
    status: false,
    discount: 0,
    unit: 0,
    product: ''
  }
  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initPromotion)))
  const promotions = ref<Promotion[]>([
    {
      id: 1,
      name: 'มาคนเดียวทำไมซื้อ3แก้วอะ',
      con: 'ถ้าซื้อครบ 3 แก้ว ลดราคาทั้งหมด 10%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.1,
      unit: 3,
      product: ''
    },
    {
      id: 2,
      name: 'มาคนเดียวทำไมซื้อ5แก้วอะ',
      con: 'ถ้าซื้อครบ 5 แก้ว ลดราคาทั้งหมด 20%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.2,
      unit: 5,
      product: ''
    }
  ])

  let lastId = promotions.value.length + 1
  function promotionUnit(r: ReceiptItem[]) {
    promotions.value.sort((a, b) => b.unit - a.unit)
    for (let index = 0; index < promotions.value.length; index++) {
      let count = 0
      if (promotions.value[index].unit != 0) {
        if (r != undefined) {
          for (let index = 0; index < r.length; index++) {
            count += r[index].unit
          }
          if (count >= promotions.value[index].unit) {
            checkDisMax(promotions.value[index])
            return
          } else {
            disMax.value = 0
            currentPromotion.value = null
          }
        }
      }
    }
  }
  function checkDisMax(p: Promotion) {
    if (p.discount > disMax.value) {
      disMax.value = p.discount
      currentPromotion.value = p
    }
  }
  function clearPromo() {
    currentPromotion.value = null
  }

  function deletePromotion() {
    console.log(editedIndex)
    promotions.value.splice(editedIndex, 1)
    dialogDelete.value = false
  }

  function closeDialog() {
    dialogDelete.value = false
    nextTick(() => {
      editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
    })
  }

  function openDialogDelete(p: Promotion) {
    editedIndex = promotions.value.indexOf(p)
    editedPromotion.value = Object.assign({}, p)
    dialogDelete.value = true
  }

  function editPromotion(p: Promotion) {
    editedIndex = promotions.value.indexOf(p)
    editedPromotion.value = Object.assign({}, p)
    dialog.value = true
  }

  function addPromotion() {
    dialog.value = true
  }

  function save() {
    if (editedPromotion.value.id > -1) {
      Object.assign(promotions.value[editedIndex], editedPromotion.value)
      closeDialogAdd()
      closeDialog()
    } else {
      editedPromotion.value.id = lastId++
      promotions.value.push(editedPromotion.value)
      closeDialogAdd()
    }
  }
  function closeDialogAdd() {
    dialog.value = false

    nextTick(() => {
      editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
    })
  }

  return {
    promotions,
    currentPromotion,
    disMax,
    dialog,
    dialogDelete,
    form,
    editedPromotion,
    promotionUnit,
    clearPromo,
    deletePromotion,
    closeDialog,
    openDialogDelete,
    editPromotion,
    addPromotion,
    save
  }
})
