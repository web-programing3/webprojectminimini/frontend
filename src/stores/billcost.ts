import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { BillCost } from '@/types/BillCost'

export const useBillCostStore = defineStore('billCost', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false) 
  let editedIndex = -1
  let lastIndex = 7
  const initBillCost:BillCost = {
    id: -1,   
    type: '',
    date: '',
    time: '',
    total: 0,
    other: '',
    }
    
  const editedbillCost = ref<BillCost>(JSON.parse(JSON.stringify (initBillCost)))
  const billCosts = ref<BillCost[]>([
    { id: 1, type: 'Electricity bill', date:'2023-12-5',time: '12:53:30', total:5000 ,other: ''},
    { id: 2, type: 'Water bill', date:'2023-12-5',time: '12:53:30',total:1000 ,other: ''},
    { id: 3, type: 'Rent bill', date:'2023-12-5',time: '12:53:30', total:10000 ,other: ''},
    { id: 4, type: 'Electricity bill', date:'2024-01-5',time: '12:53:30',total:5500 ,other: ''},
    { id: 5, type: 'Water bill', date:'2024-01-5',time: '12:53:30', total:1000 ,other: ''},
    { id: 6, type: 'Rent bill', date:'2024-01-5',time: '12:53:30', total:10000 ,other: ''},
  ])
  function openDialog(){
    dialog.value = true
    nextTick(() => {
        editedbillCost.value = Object.assign({}, initBillCost)
      editedIndex = -1
    })
  }
  // function save(){
  //   if (editedbillCost.value.id > -1){
  //    Object.assign(billCosts.value[editedIndex], editedbillCost.value)
  //  } else {
  //    editedbillCost.value.id = lastIndex++
  //    billCosts.value.push(editedbillCost.value)
  //   }
  //   closeDialog()
  //  }

   function save() {
    if (editedbillCost.value.id > -1) {
      Object.assign(billCosts.value[editedIndex], editedbillCost.value);
    } else {
      editedbillCost.value.id = lastIndex++;
      billCosts.value.unshift(editedbillCost.value); 
    }
    closeDialog();
  }
  
   function closeDialog(){
    dialog.value = false
    nextTick(() => {
        editedbillCost.value = Object.assign({}, initBillCost)
      editedIndex = -1
    })
  }
    function closeDelete() {
      dialogDelete.value = false
      nextTick(() => {
        editedbillCost.value = Object.assign({}, initBillCost)
      })
    }
  
  function deleteItem(item:BillCost) {
    editedIndex = billCosts.value.indexOf(item)
    editedbillCost.value = Object.assign({}, item)
          dialogDelete.value = true
          
  }
  function  deleteItemConfirm () {
      // delete item from List
      billCosts.value.splice(editedIndex, 1)
      closeDelete()
    }
 

  return {
    billCosts,
    openDialog,
    dialog,
    editedbillCost,
    save,
    closeDialog,
    deleteItem,
    closeDelete,
    dialogDelete,
    deleteItemConfirm
    
  }
})
