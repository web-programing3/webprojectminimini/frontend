import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'

export const useProductStore = defineStore('product', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false)
  let editedIndex = -1
  let lastIndex = 18
  const initProduct:Product = {
    id: -1,
    name: '',
    price: 0,
    category: 1,
    type: '-',
    gsize: '-',
    sweet_level: '-'
    }
    const editedProduct = ref<Product>(JSON.parse(JSON.stringify (initProduct)))
  const products = ref<Product[]>([
    { id: 1, name: 'ลาเต้', price: 35.00, category:1,type: 'CF',gsize:'SML' ,sweet_level:'-'},
    { id: 2, name: 'เอสเพรสโซ', price: 40.00,category:1,type: 'HC',gsize:'SML' ,sweet_level:'-'},
    { id: 3, name: 'คาปูชิโน่', price: 45.00, category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-'},
    { id: 4, name: 'อเมริกาโน่', price: 40.00,category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-'},
    { id: 5, name: 'มอคค่า', price: 35.00, category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },
    { id: 6, name: 'แคปปูชิโน่', price: 30.00,category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },
    { id: 7, name: 'ลัตเต่', price: 40.00, category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },
    { id: 8, name: 'กาแฟผสม', price: 50.00, category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },
    { id: 9, name: 'มัฟฟิน', price: 40.00, category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },
    { id: 10, name: 'น้ำตาล', price: 35.00, category:2,type: '-',gsize:'-' ,sweet_level:'-' },
    { id: 11, name: 'น้ำตาลทราย', price: 40.00, category:2,type: '-',gsize:'-' ,sweet_level:'-' },
    { id: 12, name: 'ครัวซอง', price: 35.00, category:3,type: '-',gsize:'-' ,sweet_level:'-'},
    { id: 13, name: 'ห้องน้ำซอง', price: 40.00, category:3,type: '-',gsize:'-' ,sweet_level:'-' },
    { id: 14, name: 'แฮร่!!', price: 45.00, category:3,type: '-',gsize:'-' ,sweet_level:'-' },
    { id: 15, name: 'อเมริกาโน่', price: 40.00,category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-'},
    { id: 16, name: 'มอคค่า', price: 35.00, category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },
    { id: 17, name: 'แคปปูชิโน่', price: 30.00,category:1,type: 'HCF',gsize:'SML' ,sweet_level:'-' },

  ])
  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  function setProductCategory(){
    products1.value =[]
    products2.value =[]
    products3.value =[]
    for (let index = 0; index < products.value.length; index++) {
      if(products.value[index].category==1){
        products1.value.push(products.value[index])
      }else if(products.value[index].category==2){
        products2.value.push(products.value[index])
      }else{
        products3.value.push(products.value[index])
      }
      
    }
  }
  function seteditedProduct(p:Product){
    editedProduct.value = p
  }
  function openDialog(){
    dialog.value = true
    nextTick(() => {
      editedProduct.value = Object.assign({}, initProduct)
      editedIndex = -1
    })
    
  }
  function closeDialog(){
    dialog.value = false
    dialogDelete.value = false
    nextTick(() => {
      editedProduct.value = Object.assign({}, initProduct)
      editedIndex = -1
    })
  }
  function save(){
    if(editedProduct.value.category==1){
      editedProduct.value.gsize = 'SML'
    }else{
      editedProduct.value.type = '-'
      editedProduct.value.gsize = '-'
    }
   if (editedProduct.value.id > -1){
    Object.assign(products.value[editedIndex], editedProduct.value)
  } else {
     editedProduct.value.id = lastIndex++
      products.value.push(editedProduct.value)
   }
    closeDialog()
    
  }
  function editProduct(p:Product){
    editedIndex = products.value.indexOf(p)
    editedProduct.value = Object.assign({}, p)
    dialog.value = true
  }
  function deleteProduct(){
    products.value.splice(editedIndex, 1)
    dialogDelete.value = false
  }
  function openDialogDelete(p:Product){
    editedIndex = products.value.indexOf(p)
    editedProduct.value = Object.assign({}, p)
    dialogDelete.value = true
}
  return { products1,products2,products3,products,dialog,dialogDelete,
          setProductCategory,editedProduct,seteditedProduct,closeDialog,openDialog,save,editProduct,openDialogDelete,deleteProduct}
})
