type Promotion = {
    id:number
    name:string
    con:string
    start:string
    end:string
    status:boolean
    discount:number
    unit:number
    product:string
}

export type {Promotion}