// type Salarybaht = 25000 | 15000 //manager|staff
// type Status = 'Paid' | 'Not Paid'
type Salary = {
    idSalary:number
    // dute:string
    // time: string
    month: string
    salarybaht: number
    status: string
}

export type {Salary}