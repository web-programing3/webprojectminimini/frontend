type Product = {
    id: number;
    name: string;
    price: number;
    category: number; // 1= drink ||2 = dessert(ขนมหวาน) || 3 = bakery
    type:string;
    gsize:string;
    sweet_level:string;
}

export{type Product }