type Stock = {
    id: number;
    name: string;
    minimum: number;
    balance: number;
    unit:string;
    price:number;
}

export{type Stock }