import type { Stock } from "@/types/Stock";
import http from "./http";

function addNew(stock:Stock){
    return  http.post('/stocks',stock)
}
function update(stock:Stock){
    return  http.patch('/stocks',stock)
}
function remove(stock:Stock){
    return  http.delete('/stocks/'+stock.id)
}
function getStock(stock:Stock){
    return  http.get('/stocks/'+stock.id)
}
function getStocks(){
    return  http.get('/stocks')
}

export default {addNew,update , remove, getStock, getStocks}